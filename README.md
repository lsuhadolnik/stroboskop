# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://lsuhadolnik@bitbucket.org/asistent/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/lsuhadolnik/stroboskop/commits/156c96aadbf4b389bc379e528875461895a940bd

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/lsuhadolnik/stroboskop/commits/bd379cc957877f4ea56c4ecb01e118a67059e99d

Naloga 6.3.2:
https://bitbucket.org/lsuhadolnik/stroboskop/commits/7f7f45a8cc5d5a84616882a21d127b10006c5743

Naloga 6.3.3:
https://bitbucket.org/lsuhadolnik/stroboskop/commits/25472c0b4507acaa1c511867953acc96fa515a30

Naloga 6.3.4:
https://bitbucket.org/lsuhadolnik/stroboskop/commits/fb63afdc4262f9e4235d66f9eb4cbb07e5dd4823

Naloga 6.3.5:
```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/lsuhadolnik/stroboskop/commits/4104671c3094d081bbf51bc85068872835864131

Naloga 6.4.2:
https://bitbucket.org/lsuhadolnik/stroboskop/commits/dd6d05aff76a28d7a428b9d8ad4255d7b0212d68

Naloga 6.4.3:
https://bitbucket.org/lsuhadolnik/stroboskop/commits/febb7a93239ad051c08dd78c0eba773003d32856

Naloga 6.4.4:
https://bitbucket.org/lsuhadolnik/stroboskop/commits/1e71fb6d119de3bc378585b2063da283e96408e0
